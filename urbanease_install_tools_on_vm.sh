#!/bin/bash

if [[ -z $1 ]]; then
    echo "Need one argument to set size of swap file."
    echo  "Ex : bash ./deploy_vm.sh 4 for 4g swap on wm"
elif [ "$EUID" -ne 0 ]; then
    echo "Please be root to run this script"
else
    echo "Starting VM Installation ..."
    apt update -y
    echo "Creating $1G file for swap ..."
    sudo fallocate -l $1G /swapfile
    echo "Chmod swapfile ..."
    ls -lh /swapfile
    sudo chmod 600 /swapfile
    ls -lh /swapfile
    echo "Assign file to swap ..."
    sudo mkswap /swapfile
    sudo swapon /swapfile
    echo "Verify ..."
    sudo swapon --show
    echo "Now add swap permanently on vm ..."
    sudo cp /etc/fstab /etc/fstab.bak
    echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
    echo "Setting swappiness"
    sudo sysctl vm.swappiness=10
    echo "Setting vm.swappiness in /etc/sysctl.conf"
    echo 'vm.swappiness=10' >> /etc/sysctl.conf
    echo "Setting Cache Pressure Setting ..."
    sudo sysctl vm.vfs_cache_pressure=50
    echo 'vm.vfs_cache_pressure=50' >> /etc/sysctl.conf
    echo "Done ! Now Installing tools"
    echo "python3-pip"
    apt install -y python3-pip
    echo "installing GDAL ..."
    sudo add-apt-repository ppa:ubuntugis/ppa -y
    apt-get update -y
    sudo apt-get install -y gdal-bin
    pip3 install GDAL==$(gdal-config --version) --global-option=build_ext --global-option="-I/usr/include/gdal"
    echo "Installing mapbox ..."
    pip3 install mapboxcli
    echo "Installing unzip ..."
    sudo apt install -y unzip
    echo "Done ! VM is ready, happy codding :)"
fi


